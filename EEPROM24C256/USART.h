/*
 * USART.h
 *
 * Created: 3/11/2022 8:49:50 AM
 *  Author: LAP068
 */ 


#ifndef USART_H_
#define USART_H_

#include "main.h"

#define USART_BAUDRATE 9600
#define BAUD_PRESCALE (((8000000 / (USART_BAUDRATE * 16UL))) - 1)

void UART_init(long UART_BAUDRATE);
unsigned char UART_RxChar();
void UART_TxChar(char ch);
void UART_SendString(char *str);


#endif /* USART_H_ */