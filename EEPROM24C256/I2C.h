#ifndef __I2C_H
#define __I2C_H
#include "main.h"



void khoi_tao_I2C();
int start_I2C(char write_address);
int repeat_start_I2C(char read_address );
int I2C_Write(char data);
char I2C_Read_Ack();
char I2C_Read_Nack();
void I2C_Stop();

#endif