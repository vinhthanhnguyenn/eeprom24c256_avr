#ifndef __EEPROM_H
#define __EEPROM_H
#include "main.h"
#include "I2C.h"

#define EEPROM_ADDR_WRITE 0xA0//1010 0000
#define EEPROM_ADDR_READ  0xA1//1010 0001
#define PAGE_SIZE  64
#define PAGE_NUMBER 512
#define WORD_NUMBER_BYTE 6 // because 9 bits for WORD_NUMBER_PAGE ,6 bits for WORD_NUMBER_BYTE

void I2C_Mem_Write(uint16_t Mem_Addr,uint8_t *Data,uint16_t Length);
void I2C_Mem_Read(uint16_t Mem_Addr,uint8_t *Data,uint16_t Length);
uint16_t Data_Seperate(uint16_t offset,uint16_t size);
void EEPROM_Write(uint16_t Page,uint16_t Offset,uint8_t *data,uint16_t Size);
void EEPROM_Read(uint16_t Page,uint16_t Offset,uint8_t *data,uint16_t Size);
void EEPROM_Errase(uint16_t Page,uint16_t Offset,uint16_t Size);




#endif