/*
 * USART.c
 *
 * Created: 3/11/2022 8:50:55 AM
 *  Author: LAP068
 */ 
#include "USART.h"

void UART_init(long UART_BAUDRATE)
{
	UCSRB |= (1 << RXCIE) | (1 << RXEN) | (1 << TXEN);	/* Turn on Tx Rx */
	UCSRC |= (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1);/* 8 bit data */
	UBRRL = UART_BAUDRATE;			/* Load baud rate 51 */
	UBRRH = (UART_BAUDRATE >> 8);		/* Load upper 8-bits*/
}

unsigned char UART_RxChar()
{
	while ((UCSRA & (1 << RXC)) == 0);/* Wait till data is received */
	return(UDR);		/* Return the byte */
}

void UART_TxChar(char ch)
{
	while (! (UCSRA & (1<<UDRE)));  /* Wait for empty transmit buffer */
	UDR = ch;
}

void UART_SendString(char *str)
{
	unsigned char j=0;
	
	while (str[j]!=0)		/* Send string till null */
	{
		UART_TxChar(str[j]);
		j++;
	}
}